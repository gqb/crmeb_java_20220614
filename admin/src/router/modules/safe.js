// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import Layout from '@/layout'

const safeRouter = {
    path: '/safe',
    component: Layout,
    redirect: '/safe/index',
    name: 'Safe',
    meta: {
      title: '隐患短信推送',
      icon: 'clipboard'
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/safe/index'),
        name: 'DangerList',
        meta: { title: '隐患查询', icon: '' }
      },
      {
        path: 'safeUser',
        component: () => import('@/views/safe/safeUser/index'),
        name: 'SafeUser',
        meta: { title: '通知设置', icon: '' }
      },
      {
        path: 'selfCheck',
        component: () => import('@/views/safe/selfCheck/index'),
        name: 'SelfCheck',
        meta: { title: '企业自查', icon: '' }
      },
      {
        path: 'cpcx',
        component: () => import('@/views/safe/cpcx/index'),
        name: 'Cpcx',
        meta: { title: '常普常新', icon: '' }
      }
      ,
      {
        path: 'sms',
        component: () => import('@/views/safe/sms/index'),
        name: 'Sms',
        meta: { title: '短信记录', icon: '' }
      }
      ,
      {
        path: 'entList',
        component: () => import('@/views/safe/enterList/index'),
        name: 'EntList',
        meta: { title: '企业列表', icon: '' }
      }
    ]
  }

export default safeRouter
