// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import request from '@/utils/request'

/**
 * 新增商品
 * @param pram
 */
export function productCreateApi(data) {
  return request({
    url: '/admin/store/product/save',
    method: 'POST',
    data
  })
}

/**
 * 编辑商品
 * @param pram
 */
export function productUpdateApi(data) {
  return request({
    url: '/admin/store/product/update',
    method: 'POST',
    data
  })
}

/**
 * 商品详情
 * @param pram
 */
export function productDetailApi(id) {
  return request({
    url: `/admin/store/product/info/${id}`,
    method: 'GET'
  })
}

export function deptList(){
  return request({
    url: '/admin/safe/dept',
    method:'get'
  })
}
export function deptListTree(cityCode){
  return request({
    url: '/admin/safe/deptTreeLocal',
    method:'get',
    params:{cityCode:cityCode}
  })
}
export function sendSafeSms(){
  return request({
    url: '/admin/safe/sendSafeSms',
    method:'get'
  })
}
export function userList(deptId){
  return request({
    url: '/admin/safe/userList',
    method:'get',
    params:{deptId:deptId}
  })
}
export function updateUser(sendSms,personId){
  return request({
    url: '/admin/safe/updateUser',
    method:'get',
    params:{sendSms:sendSms,personId:personId}
  })
}
export function dangerList(days,page,nearDayStart,nearDayEnd,cityCode){
  return request({
    url: '/admin/safe/dangerSafeRecord',
    method:'get',
    params:{days:days,page:page,nearDayStart:nearDayStart,nearDayEnd:nearDayEnd,cityCode:cityCode}
  })
}


export function deptTree(){
  return request({
    url: '/admin/safe/deptTree',
    method:'get'
  })
}
export function cpcxStatis(){
  return request({
    url: '/admin/safe/cpcxStatis',
    method:'get'
  })
}
export function cpcxEntList(cityCode,type,page){
  return request({
    url: '/admin/safe/cpcxEntList',
    method:'get',
    params:{cityCode:cityCode,type:type,page:page}
  })
}
export function sstatentselfcheck(cityCode,statMonth){
  return request({
    url: '/admin/safe/sstatentselfcheck',
    method:'get',
    params:{cityCode:cityCode,statMonth:statMonth}
  })
}
export function noselfchecklist(cityCode,statMonth,page){
  return request({
    url: '/admin/safe/noselfchecklist',
    method:'get',
    params:{cityCode:cityCode,statMonth:statMonth,page:page}
  })
}

export function smsList(page,limit,sended){
  return request({
    url: '/admin/safe/smsList',
    method:'get',
    params:{page:page,limit:limit,sended:sended}
  })
}
export function sendSms(id){
  return request({
    url: '/admin/safe/sendSms',
    method:'get',
    params:{id:id}
  })
}
export function taskSend(){
  return request({
    url: '/admin/safe/taskSend',
    method:'get'
  })
}
export function sendSmsZcManual(cityCode,statMonth){
  return request({
    url: '/admin/safe/sendSmsZcManual',
    method:'get',
    params:{cityCode:cityCode,statMonth:statMonth}
  })
}
export function sendCpcxSms(cityCode){
  return request({
    url: '/admin/safe/sendCpcxSms',
    method:'get',
    params:{cityCode:cityCode}
  })
}
export function entList(cityCode,entFullName,page,limit,sortField){
  return request({
    url: '/admin/safe/endList',
    method:'get',
    params:{cityCode:cityCode,entFullName:entFullName,page:page,limit:limit,sortField:sortField,asc:true}
  })
}
