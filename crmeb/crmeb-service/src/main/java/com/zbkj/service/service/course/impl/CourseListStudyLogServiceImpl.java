package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseListStudyLog;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListStudyLogSearchRequest;
import com.zbkj.service.dao.CourseListStudyLogDao;
import com.zbkj.service.service.course.CourseListStudyLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.Zhang
* @description CourseListStudyLogServiceImpl 接口实现
* @date 2022-07-22
*/
@Service
public class CourseListStudyLogServiceImpl extends ServiceImpl<CourseListStudyLogDao, CourseListStudyLog> implements CourseListStudyLogService {

    @Resource
    private CourseListStudyLogDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.Zhang
    * @since 2022-07-22
    * @return List<CourseListStudyLog>
    */
    @Override
    public List<CourseListStudyLog> getList(CourseListStudyLogSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseListStudyLog 类的多条件查询
        LambdaQueryWrapper<CourseListStudyLog> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseListStudyLog model = new CourseListStudyLog();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

