package com.zbkj.service.service.course.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.Course;
import com.zbkj.common.model.course.CourseList;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseSearchRequest;
import com.zbkj.common.response.course.CourseResponse;
import com.zbkj.service.dao.CourseDao;
import com.zbkj.service.service.course.CourseListService;
import com.zbkj.service.service.course.CourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Mr.Zhang
 * @description CourseServiceImpl 接口实现
 * @date 2022-07-22
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseDao, Course> implements CourseService {

    @Resource
    private CourseDao dao;

    @Autowired
    private CourseListService courseListService;

    /**
     * 列表
     *
     * @param request          请求参数
     * @param pageParamRequest 分页类参数
     * @return List<Course>
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @Override
    public List<Course> getList(CourseSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 Course 类的多条件查询
        LambdaQueryWrapper<Course> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        Course model = new Course();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

    @Override
    public CourseResponse detail(Integer id) {
        Course course = this.getById(id);
        LambdaQueryWrapper<CourseList> courseListLambdaQueryWrapper = new LambdaQueryWrapper<>();
        courseListLambdaQueryWrapper.eq(CourseList::getCourseId, id);
        List<CourseList> courseLists = courseListService.list(courseListLambdaQueryWrapper);
        CourseResponse courseResponse=new CourseResponse();
        BeanUtil.copyProperties(course,courseResponse);
        courseResponse.setCourseLists(courseLists);
        return courseResponse;
    }


}

