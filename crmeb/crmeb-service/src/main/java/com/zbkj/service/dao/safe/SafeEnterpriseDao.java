package com.zbkj.service.dao.safe;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.safe.SafeEnterprise;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-10-27
 */
public interface SafeEnterpriseDao extends BaseMapper<SafeEnterprise> {

}
