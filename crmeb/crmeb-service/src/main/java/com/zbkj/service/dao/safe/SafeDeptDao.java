package com.zbkj.service.dao.safe;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.safe.SafeDept;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-11-11
 */
public interface SafeDeptDao extends BaseMapper<SafeDept> {

}
