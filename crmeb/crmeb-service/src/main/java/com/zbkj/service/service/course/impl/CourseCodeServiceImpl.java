package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseCode;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseCodeSearchRequest;
import com.zbkj.service.dao.CourseCodeDao;
import com.zbkj.service.service.course.CourseCodeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.Zhang
* @description CourseCodeServiceImpl 接口实现
* @date 2022-07-22
*/
@Service
public class CourseCodeServiceImpl extends ServiceImpl<CourseCodeDao, CourseCode> implements CourseCodeService {

    @Resource
    private CourseCodeDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.Zhang
    * @since 2022-07-22
    * @return List<CourseCode>
    */
    @Override
    public List<CourseCode> getList(CourseCodeSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseCode 类的多条件查询
        LambdaQueryWrapper<CourseCode> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseCode model = new CourseCode();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

