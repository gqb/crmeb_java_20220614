package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseProduction;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionSearchRequest;
import com.zbkj.service.dao.CourseProductionDao;
import com.zbkj.service.service.course.CourseProductionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.Zhang
* @description CourseProductionServiceImpl 接口实现
* @date 2022-07-22
*/
@Service
public class CourseProductionServiceImpl extends ServiceImpl<CourseProductionDao, CourseProduction> implements CourseProductionService {

    @Resource
    private CourseProductionDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.Zhang
    * @since 2022-07-22
    * @return List<CourseProduction>
    */
    @Override
    public List<CourseProduction> getList(CourseProductionSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseProduction 类的多条件查询
        LambdaQueryWrapper<CourseProduction> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseProduction model = new CourseProduction();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

