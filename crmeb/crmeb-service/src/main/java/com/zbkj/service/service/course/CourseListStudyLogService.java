package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseListStudyLog;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListStudyLogSearchRequest;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseListStudyLogService 接口
* @date 2022-07-22
*/
public interface CourseListStudyLogService extends IService<CourseListStudyLog> {

    List<CourseListStudyLog> getList(CourseListStudyLogSearchRequest request, PageParamRequest pageParamRequest);
}