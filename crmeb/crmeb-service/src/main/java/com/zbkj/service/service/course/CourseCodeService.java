package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseCode;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseCodeSearchRequest;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseCodeService 接口
* @date 2022-07-22
*/
public interface CourseCodeService extends IService<CourseCode> {

    List<CourseCode> getList(CourseCodeSearchRequest request, PageParamRequest pageParamRequest);
}