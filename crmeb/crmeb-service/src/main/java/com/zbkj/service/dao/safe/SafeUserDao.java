package com.zbkj.service.dao.safe;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.safe.SafeUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-10-28
 */
public interface SafeUserDao extends BaseMapper<SafeUser> {

}
