package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseList;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListSearchRequest;
import com.zbkj.service.dao.CourseListDao;
import com.zbkj.service.service.course.CourseListService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.Zhang
* @description CourseListServiceImpl 接口实现
* @date 2022-07-22
*/
@Service
public class CourseListServiceImpl extends ServiceImpl<CourseListDao, CourseList> implements CourseListService {

    @Resource
    private CourseListDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.Zhang
    * @since 2022-07-22
    * @return List<CourseList>
    */
    @Override
    public List<CourseList> getList(CourseListSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseList 类的多条件查询
        LambdaQueryWrapper<CourseList> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseList model = new CourseList();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

