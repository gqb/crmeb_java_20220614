package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseProductionImg;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionImgSearchRequest;
import com.zbkj.service.dao.CourseProductionImgDao;
import com.zbkj.service.service.course.CourseProductionImgService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.Zhang
* @description CourseProductionImgServiceImpl 接口实现
* @date 2022-07-22
*/
@Service
public class CourseProductionImgServiceImpl extends ServiceImpl<CourseProductionImgDao, CourseProductionImg> implements CourseProductionImgService {

    @Resource
    private CourseProductionImgDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.Zhang
    * @since 2022-07-22
    * @return List<CourseProductionImg>
    */
    @Override
    public List<CourseProductionImg> getList(CourseProductionImgSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseProductionImg 类的多条件查询
        LambdaQueryWrapper<CourseProductionImg> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseProductionImg model = new CourseProductionImg();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

