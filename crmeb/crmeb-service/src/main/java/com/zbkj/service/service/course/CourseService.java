package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.Course;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseSearchRequest;
import com.zbkj.common.response.course.CourseResponse;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseService 接口
* @date 2022-07-22
*/
public interface CourseService extends IService<Course> {

    List<Course> getList(CourseSearchRequest request, PageParamRequest pageParamRequest);

    CourseResponse detail(Integer id);
}