package com.zbkj.service.dao.safe;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.safe.SafeRecord;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-10-27
 */
public interface SafeRecordDao extends BaseMapper<SafeRecord> {

}
