package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseProduction;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionSearchRequest;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseProductionService 接口
* @date 2022-07-22
*/
public interface CourseProductionService extends IService<CourseProduction> {

    List<CourseProduction> getList(CourseProductionSearchRequest request, PageParamRequest pageParamRequest);
}