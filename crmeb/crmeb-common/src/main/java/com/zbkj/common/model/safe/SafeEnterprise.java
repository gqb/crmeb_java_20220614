package com.zbkj.common.model.safe;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-10-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SafeEnterprise对象", description="")
public class SafeEnterprise implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "企业id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String entFullName;

    private String entId;

    private String cityName;

    private String industryTypeName;

    private String industryTypeCode;

    private String principal;

    private String principalMphone;

    private String cityCode;

    private String surveyStatusName;

    private Date lastOftencheckDate;
    private Date selfCheckDate;

    private String partakeStatusName;

    private Boolean isDelete;

    private Date syncTime;

    /**
     * 当月自查隐患数量，目前是要大于2条的
     */
    private Integer selfCheckCount;


}
