package com.zbkj.common.model.safe;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-11-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SafeDept对象", description="")
public class SafeDept implements Serializable {

    private static final long serialVersionUID=1L;

    private String id;

    private String pid;

    private String extend03;

    private String label;

    private String extend02;

    private Boolean leaf;

    private String extend01;

    private String extend;

    private String expand;

    private String checked;

    private String text;

    private String value;

    private Boolean sendSms;

    private String token;

    @TableField(exist = false)
    private List<SafeDept> children=new ArrayList<>();

}
