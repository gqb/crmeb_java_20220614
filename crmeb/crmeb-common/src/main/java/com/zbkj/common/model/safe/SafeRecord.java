package com.zbkj.common.model.safe;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-10-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SafeRecord对象", description="")
public class SafeRecord implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "企业id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String hdId;

    private String checkId;

    private String checkItemId;

    private String checkTempId;

    private String entId;

    private String entFullName;

    private String hdDesc;

    private String hdLevel;

    private String hdLevelName;

    private String hdLocation;

    private String hdSource;

    private String hdSourceName;

    private String hdType;

    private String hdTypeName;

    private String neateType;

    private String neateTypeName;

    private String neatenLimitDate;


    private String checkerName;

    private String checkTime;

    private String prodAddrCode;

    private String prodAddrName;

    private String industryTypeCode;

    private String industryTypeName;

    private String domain;

    private String domainName;

    private String checkKind;

    private String checkAscnId;

    private String checkDeptId;

    private String neatenSituation;
    private String neatenSituationName;
    private String jsonStr;


}
