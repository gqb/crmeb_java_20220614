package com.zbkj.common.model.safe;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-10-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SafeUser对象", description="")
public class SafeUser implements Serializable {

    private static final long serialVersionUID=1L;

    private String personId;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String ascnId;

    private String ascnName;

    private String defaultDeptName;

    private String deptFullName;

    private String deptId;

    private String deptIds;

    private String deptSimpleName;

    private String personName;

    private String personNo;

    private String sex;

    private String sexName;

    private String jsonStr;
    private String phone;

    private Boolean sendSms;

    private String cityCode;
}
