package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.CourseCode;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseCodeRequest;
import com.zbkj.common.request.course.CourseCodeSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.CourseCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course-code")
@Api(tags = "") //配合swagger使用

public class CourseCodeController {

    @Autowired
    private CourseCodeService courseCodeService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseCode>> getList(@Validated CourseCodeSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseCode> courseCodeCommonPage = CommonPage.restPage(courseCodeService.getList(request, pageParamRequest));
        return CommonResult.success(courseCodeCommonPage);
    }

    /**
     * 新增
     * @param courseCodeRequest 新增参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated CourseCodeRequest courseCodeRequest){
        CourseCode courseCode = new CourseCode();
        BeanUtils.copyProperties(courseCodeRequest, courseCode);

        if(courseCodeService.save(courseCode)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(courseCodeService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param id integer id
     * @param courseCodeRequest 修改参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @Validated CourseCodeRequest courseCodeRequest){
        CourseCode courseCode = new CourseCode();
        BeanUtils.copyProperties(courseCodeRequest, courseCode);
        courseCode.setId(id);

        if(courseCodeService.updateById(courseCode)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseCode> info(@RequestParam(value = "id") Integer id){
        CourseCode courseCode = courseCodeService.getById(id);
        return CommonResult.success(courseCode);
   }
}



