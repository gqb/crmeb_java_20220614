package com.zbkj.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.safe.SafeEnterprise;

import java.util.List;

/**
* @author Mr.guqianbin
* @description SafeEnterpriseService 接口
* @date 2022-10-27
*/
public interface SafeEnterpriseService extends IService<SafeEnterprise> {

}