package com.zbkj.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.admin.service.SafeRecordService;
import com.zbkj.common.model.safe.SafeRecord;
import com.zbkj.service.dao.safe.SafeRecordDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author Mr.guqianbin
* @description SafeRecordServiceImpl 接口实现
* @date 2022-10-27
*/
@Service
public class SafeRecordServiceImpl extends ServiceImpl<SafeRecordDao, SafeRecord> implements SafeRecordService {

    @Resource
    private SafeRecordDao dao;




}

