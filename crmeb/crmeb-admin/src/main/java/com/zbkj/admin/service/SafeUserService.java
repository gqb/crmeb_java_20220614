package com.zbkj.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.safe.SafeUser;

import java.util.List;

/**
* @author Mr.guqianbin
* @description SafeUserService 接口
* @date 2022-10-28
*/
public interface SafeUserService extends IService<SafeUser> {

}