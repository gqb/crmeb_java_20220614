package com.zbkj.admin.service.impl;

public class AliSmsConstants {

    public static final String SMS_SEND_SUCCESS_CODE="OK";
    public static final Integer SMS_SEND_SUCCESS_CODE_INT=100;
    public static final Integer SMS_SEND_FAIL_CODE_INT=130;
    /**
     * 短信登录验证码
     */
    public static final String SMS_CONFIG_TYPE_VERIFICATION_CODE="SMS_221082343";

    /**
     * 订单支付完成短信模版
     */
    public static final String SMS_ORDER_PAY_SUCCESS_CODE="SMS_216838729";

    /**
     * 发货完成短信
     */
    public static final String SMS_DELIVER_GOODS_SUCCESS_CODE="SMS_216828897";

    /**
     * 购物积分增加短信
     */
    public static final String SMS_INTEGRAL_ADD_CODE="SMS_219741429";

    /**
     * 推广积分增加短信
     */
    public static final String SMS_SPREAD_INTEGRAL_ADD_CODE="SMS_219741415";

    /**
     * 积分分红短信增值通知
     */
    public static final String SMS_SPREAD_INTEGRAL_BONUS_CODE="SMS_219750914";
}
