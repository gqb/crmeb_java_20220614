package com.zbkj.admin.controller.safe;

import cn.hutool.core.util.ObjectUtil;
import com.zbkj.common.config.CrmebConfig;
import com.zbkj.common.model.system.SystemAttachment;
import com.zbkj.service.service.SystemAttachmentService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("")
@Api(tags = "隐患提醒服务文件下载")
public class DownloadController {

    @Autowired
    private SystemAttachmentService attachmentService;
    @Autowired
    private CrmebConfig crmebConfig;
    @GetMapping("/down")
    public void downloadExport(Integer fId, HttpServletResponse response) {

        SystemAttachment attachment= attachmentService.getById(fId);
        if(ObjectUtil.isEmpty(attachment)){
            return;
        }
        response.setHeader("Content-Disposition", "attachment;fileName=" + attachment.getName());
        File file = new File(crmebConfig.getImagePath()+attachment.getSattDir());
        ServletOutputStream outputStream;
        byte[] array;
        try {
            outputStream = response.getOutputStream();
            array = FileUtils.readFileToByteArray(file);
            outputStream.write(array);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            return;
        }
    }

}
