package com.zbkj.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.safe.SafeRecord;

import java.util.List;

/**
* @author Mr.guqianbin
* @description SafeRecordService 接口
* @date 2022-10-27
*/
public interface SafeRecordService extends IService<SafeRecord> {

}