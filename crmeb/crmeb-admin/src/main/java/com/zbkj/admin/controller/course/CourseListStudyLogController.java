package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.CourseListStudyLog;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListStudyLogRequest;
import com.zbkj.common.request.course.CourseListStudyLogSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.CourseListStudyLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course-list-study-log")
@Api(tags = "") //配合swagger使用

public class CourseListStudyLogController {

    @Autowired
    private CourseListStudyLogService courseListStudyLogService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseListStudyLog>>  getList(@Validated CourseListStudyLogSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseListStudyLog> courseListStudyLogCommonPage = CommonPage.restPage(courseListStudyLogService.getList(request, pageParamRequest));
        return CommonResult.success(courseListStudyLogCommonPage);
    }

    /**
     * 新增
     * @param courseListStudyLogRequest 新增参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated CourseListStudyLogRequest courseListStudyLogRequest){
        CourseListStudyLog courseListStudyLog = new CourseListStudyLog();
        BeanUtils.copyProperties(courseListStudyLogRequest, courseListStudyLog);

        if(courseListStudyLogService.save(courseListStudyLog)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(courseListStudyLogService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param id integer id
     * @param courseListStudyLogRequest 修改参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @Validated CourseListStudyLogRequest courseListStudyLogRequest){
        CourseListStudyLog courseListStudyLog = new CourseListStudyLog();
        BeanUtils.copyProperties(courseListStudyLogRequest, courseListStudyLog);
        courseListStudyLog.setId(id);

        if(courseListStudyLogService.updateById(courseListStudyLog)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseListStudyLog> info(@RequestParam(value = "id") Integer id){
        CourseListStudyLog courseListStudyLog = courseListStudyLogService.getById(id);
        return CommonResult.success(courseListStudyLog);
   }
}



