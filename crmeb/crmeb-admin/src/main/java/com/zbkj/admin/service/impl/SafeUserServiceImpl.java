package com.zbkj.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.admin.service.SafeUserService;
import com.zbkj.common.model.safe.SafeUser;
import com.zbkj.service.dao.safe.SafeUserDao;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description SafeUserServiceImpl 接口实现
* @date 2022-10-28
*/
@Service
public class SafeUserServiceImpl extends ServiceImpl<SafeUserDao, SafeUser> implements SafeUserService {

    @Resource
    private SafeUserDao dao;



}

