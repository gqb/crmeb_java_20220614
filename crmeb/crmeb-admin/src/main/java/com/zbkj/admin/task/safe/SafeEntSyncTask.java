package com.zbkj.admin.task.safe;

import com.zbkj.admin.service.SafeDeptService;
import com.zbkj.admin.service.SafeEnterpriseService;
import com.zbkj.admin.service.SafeService;
import com.zbkj.common.model.safe.SafeDept;
import com.zbkj.common.model.safe.SafeEnterprise;
import com.zbkj.common.model.safe.SafeRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Configuration //读取配置
@EnableScheduling // 2.开启定时任务
@Slf4j
public class SafeEntSyncTask {
    @Autowired
    private SafeService safeService;
    @Autowired
    private SafeDeptService safeDeptService;
    @Autowired
    private SafeEnterpriseService enterpriseService;

//    @Scheduled(cron = "0 0 8 * * ?")
//    @Scheduled(fixedDelay = 1000 * 5700L)
    public void init() {
        long start=System.currentTimeMillis();
        log.info("开始企业信息同步===========================");
        sync();
        long end=System.currentTimeMillis()-start;
        log.info("结束企业信息同步===========================用时："+end/1000);
    }

    public void sync() {
        List<SafeDept> safeDepts = safeDeptService.getChildrenDeptTree("3302").get(0).getChildren();
        safeDepts.forEach(dept -> {
//                String token = dept.getToken();
//                safeService.syncEnterpriseList(dept.getExtend02());
//                safeService.syncEnterSelfCheckDate(dept.getExtend02());
//                safeService.syncEnterLastOftenCheckDate(dept.getExtend02());
//            }else {
            //仅街道一级更新
            dept.getChildren().forEach(streetDept -> {
                if (streetDept.getSendSms()) {
                    String token = streetDept.getToken();
//                    safeService.syncEnterpriseList(streetDept.getExtend02());
                    safeService.syncEnterSelfCheckDate(streetDept.getExtend02());
                    safeService.syncEnterLastOftenCheckDate(streetDept.getExtend02());
                }
            });

        });
    }
}
