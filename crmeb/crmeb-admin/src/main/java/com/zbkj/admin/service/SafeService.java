package com.zbkj.admin.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zbkj.common.model.safe.SafeEnterprise;
import com.zbkj.common.model.safe.SafeRecord;
import com.zbkj.common.model.safe.SafeUser;
import com.zbkj.common.model.sms.SmsRecord;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.safe.SafeEnterpriseRequest;

import java.util.List;
import java.util.Map;

public interface SafeService {

    public List<SafeEnterprise> syncEnterpriseList(String cityCode);
    /**
     * 常普常新日期同步
     */
    public void syncEnterLastOftenCheckDate(String cityCode);

    public  JSONObject enterSelfCheckDangeList(String cityCode, int page
            , int pageSize, String beginDateStr, String endDateStr, String token,String entName);
    public void syncEnterSelfCheckDate(String cityCode);
    public void removeTodaySendSmsPhone(List<SmsRecord> smsRecords, Integer smsType);
    /**
     * 企业隐患查询
     * @param hdSource
     * @param checkTimeStart
     * @param checkTimeEnd
     * @param current
     */
    public Map<String,Object> enterCheckDangerList(String hdSource,String checkTimeStart,String checkTimeEnd ,String current);
    public CommonPage<SafeEnterprise> entList(SafeEnterpriseRequest request, PageParamRequest pageParamRequest);


    public Map<String,Object> dangerSafeRecord(Integer days,Integer page,Integer pages,int nearDayStart
            ,int nearDayEnd,String token,String entFullName);
    public Map<String,Object> dangerSafeRecordOld(Integer days,Integer page,int nearDayStart
            ,int nearDayEnd,String token,String entFullName);
    public Map<String,Object> dangerSafeRecordAll(Integer days,Integer page,int nearDayStart
            ,int nearDayEnd,String token,String entFullName);

    public void syncSafeUser(String cityCode);
    public void updateUser(String personId,Boolean sendSms);
    public JSONArray getDept();
    public void sendSafeSms();
    public  List<Map.Entry<String, Integer>> shhfw();
    /**
     * 90天一次，提前一周提醒
     */
    public void sendCpcxSms(String cityCode,String type);

    /**
     * 企业自查提醒 30天一次
     */
    public void sendEnterZc(String cityCode,String statMonth,String type);
    public List<SafeUser> getUserList(String deptId);

    public void updateUser(Boolean sendSms,String personId);


    /**
     * 获取行政区划上下级
     * @return
     */
    public JSONArray deptTree();

    /**
     * 企业自查
     * @return
     */
    public JSONArray sstatentselfcheck(String cityCode,String statMonth);

    public JSONObject no_selfcheck_list(String cityCode,String statMonth,int page);


    public JSONArray cpcxStatis();

    /**
     * 常普常新临近企业
     * @return
     */
    public JSONObject cpcxEntList(String cityCode,String type,int page);

    public CommonPage<SmsRecord> smsList(PageParamRequest pageParamRequest);

    public String sendSms(String phone, String content);

    String exportSafe(String cityCode, int cpcxEntRemindDay,int cpcxGovDay
            ,int selfCheckEntRemindDay,int selfCheckGovDay
            ,int dangerEntRemindDay,int dangerGovDay);

}
