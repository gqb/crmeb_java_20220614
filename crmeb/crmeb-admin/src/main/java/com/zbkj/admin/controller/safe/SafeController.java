package com.zbkj.admin.controller.safe;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zbkj.admin.service.AdminLoginService;
import com.zbkj.admin.service.SafeDeptService;
import com.zbkj.admin.service.SafeService;
import com.zbkj.admin.task.safe.SafeEntSyncTask;
import com.zbkj.admin.task.safe.SafeTask;
import com.zbkj.common.model.course.CourseCode;
import com.zbkj.common.model.safe.SafeDept;
import com.zbkj.common.model.safe.SafeEnterprise;
import com.zbkj.common.model.safe.SafeRecord;
import com.zbkj.common.model.safe.SafeUser;
import com.zbkj.common.model.sms.SmsRecord;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.SystemAdminLoginRequest;
import com.zbkj.common.request.course.CourseCodeSearchRequest;
import com.zbkj.common.request.safe.SafeEnterpriseRequest;
import com.zbkj.common.response.*;
import com.zbkj.common.utils.CrmebUtil;
import com.zbkj.service.service.SmsRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 管理端登录服务
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Slf4j
@RestController
@RequestMapping("api/admin/safe")
@Api(tags = "隐患短信提醒服务")
public class SafeController {


    @Autowired
    private SafeService safeService;
    @Autowired
    private SafeDeptService safeDeptService;

    @Autowired
    private SafeTask safeTask;

    @Autowired
    private SmsRecordService smsRecordService;
    @Autowired
    private SafeEntSyncTask safeEntSyncTask;
    @ApiOperation(value="发送隐患短信")
    @GetMapping(value = "/sendSafeSms", produces = "application/json")
    public CommonResult<String> sendSafeSms() {
        safeService.sendSafeSms();
        return CommonResult.success();
    }

    @ApiOperation(value="手动发送自查提醒短信")
    @GetMapping(value = "/sendSmsZcManual", produces = "application/json")
    public CommonResult<String> sendSmsZcManual(String cityCode,String statMonth) {
        safeService.sendEnterZc(cityCode,statMonth,"manual");
        return CommonResult.success();
    }
    @ApiOperation(value="手动发送常普常新提醒短信")
    @GetMapping(value = "/sendCpcxSms", produces = "application/json")
    public CommonResult<String> sendCpcxSms(String cityCode) {
        safeService.sendCpcxSms(cityCode,"manual");
        return CommonResult.success();
    }
    @ApiOperation(value="社会化服务")
    @GetMapping(value = "/shhfw", produces = "application/json")
    public CommonResult<List<Map.Entry<String, Integer>>> shhfw() {
        ;
        return CommonResult.success(safeService.shhfw());
    }
    @ApiOperation(value="用户同步")
    @GetMapping(value = "/syncUser", produces = "application/json")
    public CommonResult<String> syncUser(String cityCode) {
        safeService.syncSafeUser(cityCode);
        return CommonResult.success();
    }
    @ApiOperation(value="用户发送短信设置")
    @GetMapping(value = "/updateUser", produces = "application/json")
    public CommonResult<String> updateUser(String personId,Boolean sendSms) {
        safeService.updateUser(personId,sendSms);
        return CommonResult.success();
    }
    @ApiOperation(value="企业同步")
    @GetMapping(value = "/syncEnterprise", produces = "application/json")
    public CommonResult<List<SafeEnterprise>> syncEnterprise(String cityCode) {
        return CommonResult.success(safeService.syncEnterpriseList(cityCode), "sync success");
    }
    @ApiOperation(value="企业自查时间同步")
    @GetMapping(value = "/syncEnterSelfCheckDate", produces = "application/json")
    public CommonResult<String> syncEnterSelfCheckDate(String cityCode) {
        safeService.syncEnterSelfCheckDate(cityCode);
        return CommonResult.success("sync success");
    }
    @ApiOperation(value="企业列表")
    @GetMapping(value = "/endList", produces = "application/json")
    public CommonResult<CommonPage<SafeEnterprise>> entList(SafeEnterpriseRequest request, @Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(safeService.entList(request,pageParamRequest));
    }

    @ApiOperation(value="常普常新信息同步")
    @GetMapping(value = "/syncEnterpriseCpcx", produces = "application/json")
    public CommonResult<String> syncEnterpriseCpcx(String cityCode) {
        safeService.syncEnterLastOftenCheckDate(cityCode);
        return CommonResult.success( "sync success");
    }
    @ApiOperation(value="获取近期未整改隐患")
    @GetMapping(value = "/dangerSafeRecord", produces = "application/json")
    public CommonResult<Map<String,Object>> dangerSafeRecord( Integer days, Integer page,int nearDayStart,int nearDayEnd,String cityCode) {
        String id=safeDeptService.comparePid(cityCode);
        SafeDept dept=safeDeptService.getById(id);
        return CommonResult.success(safeService.dangerSafeRecordOld(days,page,nearDayStart,nearDayEnd,dept.getToken(),null), "success");
    }


//    @ApiOperation(value="更新短信发送")
//    @GetMapping(value = "/updateUser", produces = "application/json")
//    public CommonResult<String> updateUser( Boolean sendSms, String personId) {
//        safeService.updateUser(sendSms,personId);
//        return CommonResult.success("success");
//    }
    @ApiOperation(value="获取用户列表")
    @GetMapping(value = "/userList", produces = "application/json")
    public CommonResult<List<SafeUser>> userList(String deptId) {
        return CommonResult.success(safeService.getUserList(deptId));
    }

    @ApiOperation(value="获取部门列表")
    @GetMapping(value = "/dept", produces = "application/json")
    public CommonResult<JSONArray> dept() {
        return CommonResult.success(safeService.getDept());
    }

    @ApiOperation(value="获取部门树")
    @GetMapping(value = "/deptTree", produces = "application/json")
    public CommonResult<JSONArray> deptTree() {
        return CommonResult.success(safeService.deptTree());
    }
    @ApiOperation(value="获取部门树")
    @GetMapping(value = "/deptTreeLocal", produces = "application/json")
    public CommonResult<List<SafeDept>> deptTreeLocal(String cityCode) {
        return CommonResult.success(safeDeptService.getChildrenDeptTree(cityCode));
    }
    @ApiOperation(value="企业自查统计")
    @GetMapping(value = "/sstatentselfcheck", produces = "application/json")
    public CommonResult<JSONArray> sstatentselfcheck(String cityCode,String statMonth) {
        return CommonResult.success(safeService.sstatentselfcheck(cityCode,statMonth));
    }

    @ApiOperation(value="企业未自查列表")
    @GetMapping(value = "/noselfchecklist", produces = "application/json")
    public CommonResult<JSONObject> noselfchecklist(String cityCode, String statMonth, int page) {
        return CommonResult.success(safeService.no_selfcheck_list(cityCode,statMonth,page));
    }

    @ApiOperation(value="常普常新统计数据")
    @GetMapping(value = "/cpcxStatis", produces = "application/json")
    public CommonResult<JSONArray> cpcxStatis() {
        return CommonResult.success(safeService.cpcxStatis());
    }
    @ApiOperation(value="常普常新统计数据")
    @GetMapping(value = "/cpcxEntList", produces = "application/json")
    public CommonResult<JSONObject> cpcxEntList(String cityCode, String type, int page) {
        return CommonResult.success(safeService.cpcxEntList(cityCode,type,page));
    }

    @ApiOperation(value = "短信发送记录") //配合swagger使用
    @RequestMapping(value = "/smsList", method = RequestMethod.GET)
    public CommonResult<CommonPage<SmsRecord>> getList(@Validated PageParamRequest pageParamRequest){
        return CommonResult.success(safeService.smsList(pageParamRequest));
    }

    @ApiOperation(value = "短信发送任务手动发送") //配合swagger使用
    @RequestMapping(value = "/taskSend", method = RequestMethod.GET)
    public CommonResult<String> taskSend(){
        safeTask.sendSms();
        return CommonResult.success("成功");
    }


    @ApiOperation(value = "同步所有企业") //配合swagger使用
    @RequestMapping(value = "/syncAllEnter", method = RequestMethod.GET)
    public CommonResult<String> syncAllEnter(){
        safeEntSyncTask.sync();
        return CommonResult.success("成功");
    }

    @ApiOperation(value = "发送短信") //配合swagger使用
    @RequestMapping(value = "/sendSms", method = RequestMethod.GET)
    public CommonResult<String> sendSms(Integer id){
        SmsRecord smsRecord= smsRecordService.getById(id);
        if(ObjectUtil.isEmpty(smsRecord)||smsRecord.getSended()){
            return CommonResult.success();
        }

        smsRecord.setResultStr(safeService.sendSms(smsRecord.getPhone(),smsRecord.getContent()));
        JSONObject jsonObject= JSON.parseObject(smsRecord.getResultStr());
        if(jsonObject.getString("status").equals("success")){
            smsRecord.setSended(true);
        }
        smsRecordService.updateById(smsRecord);

        return CommonResult.success();
    }
}
