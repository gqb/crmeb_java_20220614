package com.zbkj.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.admin.service.SafeEnterpriseService;
import com.zbkj.common.model.safe.SafeEnterprise;
import com.zbkj.service.dao.safe.SafeEnterpriseDao;
import org.springframework.stereotype.Service;

/**
* @author Mr.guqianbin
* @description SafeEnterpriseServiceImpl 接口实现
* @date 2022-10-27
*/
@Service
public class SafeEnterpriseServiceImpl extends ServiceImpl<SafeEnterpriseDao, SafeEnterprise> implements SafeEnterpriseService {

    private SafeEnterpriseDao dao;



}

