package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.CourseList;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListRequest;
import com.zbkj.common.request.course.CourseListSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.CourseListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course-list")
@Api(tags = "") //配合swagger使用

public class CourseListController {

    @Autowired
    private CourseListService courseListService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseList>> getList(@Validated CourseListSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseList> courseListCommonPage = CommonPage.restPage(courseListService.getList(request, pageParamRequest));
        return CommonResult.success(courseListCommonPage);
    }

    /**
     * 新增
     * @param courseListRequest 新增参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated CourseListRequest courseListRequest){
        CourseList courseList = new CourseList();
        BeanUtils.copyProperties(courseListRequest, courseList);

        if(courseListService.save(courseList)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(courseListService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param courseListRequest 修改参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@Validated CourseListRequest courseListRequest){
        CourseList courseList = new CourseList();
        BeanUtils.copyProperties(courseListRequest, courseList);

        if(courseListService.updateById(courseList)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseList> info(@RequestParam(value = "id") Integer id){
        CourseList courseList = courseListService.getById(id);
        return CommonResult.success(courseList);
   }
}



