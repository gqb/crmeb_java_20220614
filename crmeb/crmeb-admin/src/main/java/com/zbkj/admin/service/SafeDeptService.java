package com.zbkj.admin.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.safe.SafeDept;

import java.util.List;

/**
* @author Mr.guqianbin
* @description SafeDeptService 接口
* @date 2022-11-11
*/
public interface SafeDeptService extends IService<SafeDept> {

    public void syncDept(JSONObject jsonObject);
    public List<SafeDept> getChildrenDeptTree(String cityCode);
    public List<SafeDept> getAreaDeptTree(String cityCode);

    public String comparePid(String cityCode);

    public SafeDept getParent(String cityCode);
}