package com.zbkj.front.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.token.FrontTokenComponent;
import com.zbkj.common.utils.RequestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

/**
 *  移动端管理端 token验证拦截器 使用前注意需要一个@Bean手动注解，否则注入无效
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */
public class FrontTokenInterceptor implements HandlerInterceptor {

    @Autowired
    private FrontTokenComponent frontTokenComponent;
    private static final Logger logger = LoggerFactory.getLogger(FrontTokenInterceptor.class);

    //程序处理之前需要处理的业务
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        response.setCharacterEncoding("UTF-8");
//        logRequest(request,handler);
        String token = frontTokenComponent.getToken(request);
        if(token == null || token.isEmpty()){
            //判断路由，部分路由不管用户是否登录都可以访问
            boolean result = frontTokenComponent.checkRouter(RequestUtil.getUri(request));
            if(result){
                return true;
            }
            response.getWriter().write(JSONObject.toJSONString(CommonResult.unauthorized()));
            return false;
        }

        Boolean result = frontTokenComponent.check(token, request);
        if(!result){
            response.getWriter().write(JSONObject.toJSONString(CommonResult.unauthorized()));
            return false;
        }
        return true;
    }

    private void logRequest(HttpServletRequest request,Object handler){
        StringBuilder sb = new StringBuilder(1000);
        String url =request.getRequestURL().toString();
        if(!url.contains(".css") && !url.contains(".js") && !url.contains(".png") &&  !url.contains(".jpg")) {
            logger.info("*******************访问地址："+url+"*******************");
            logger.info("*******************客户端请求地址和操作端口：" + request.getRemoteAddr() + ":" + request.getRemotePort() + "*******************");
        }
        //获取请求参数
        Enumeration em = request.getParameterNames();
        JSONObject data = new JSONObject();
        while (em.hasMoreElements()) {
            String name = (String) em.nextElement();
            String value = request.getParameter(name);
            data.put(name,value);
        }
        sb .append("-------------------------------------------------------------\n");
        HandlerMethod h = (HandlerMethod) handler;
        sb.append("Controller: ").append(h.getBean().getClass().getName()).append("\n");
        sb.append("Method    : ").append(h.getMethod().getName()).append("\n");
        sb.append("Params    : ").append(data).append("\n");
        sb.append("URI       : ").append(request.getRequestURI()).append("\n");
        sb.append("URL       : ").append(request.getRequestURL()).append("\n");
        sb .append("-------------------------------------------------------------\n");
        logger.info(sb.toString());


    }
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

    }

}
